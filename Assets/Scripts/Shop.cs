﻿
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;
using UnityEngine.UI;

public class Shop : Singleton<Shop>
{
    protected Shop() { }

    Animator animator;

    public Text coins;
    public Button watchVideoButton;
    bool shown = false;

    int spendAmount = -1;
    private UnityEvent shopCallback;
    
    void Start()
    {
        animator = GetComponent<Animator>();
#if UNITY_EDITOR
        Advertisement.Initialize("1048645", true);
#else
        Advertisement.Initialize("1048645", false);
#endif

    }

    public void ShowShop()
    {

        if (animator != null)
        {
            animator.SetTrigger("Show");
            shown = true;
        }
    }

    public void ShowShop(UnityEvent shopCallback)
    {
        this.shopCallback = shopCallback;
        ShowShop();
    }

    public void HideShop()
    {
        if (animator == null)
            return;
        if (shown)
        {
            shown = false;
            animator.SetTrigger("Hide");
        }
    }

    void Update()
    {
        if (coins != null)
            coins.text = "Coins: " + GameScore.Instance.Coins;
        if (watchVideoButton != null)
            watchVideoButton.interactable = Advertisement.IsReady("get30Coins");

    }

    bool CanSpendCoins(int amount)
    {
        return GameScore.Instance.Coins >= amount;
    }

    void SpendCoins()
    {
        if (spendAmount <= 0)
            return;
        GameScore.Instance.Coins -= spendAmount;
        spendAmount = -1;
    }

    void SpendCoins(int amount)
    {
        GameScore.Instance.Coins -= amount;
    }

    public void SpendCoins(int amount, UnityEvent callback)
    {
        if (CanSpendCoins(amount))
        {
            SpendCoins(amount);
            callback.Invoke();
        }
        else
        {
            spendAmount = amount;
            callback.AddListener(SpendCoins);
            ShowShop(callback);
        }
    }

    void EarnCoins(int amount)
    {
        GameScore.Instance.Coins += 30;
    }

    public void WatchAd()
    {
        if (Advertisement.IsReady("get30Coins"))
        {
            var options = new ShowOptions { resultCallback = HandleAdResult };
            Advertisement.Show("get30Coins", options);
        }
    }
    void HandleAdResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                EarnCoins(30);
                HideShop();
                if (shopCallback == null)
                    return;
                shopCallback.Invoke();
                shopCallback = null;
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}
