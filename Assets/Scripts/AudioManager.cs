﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

public class AudioManager : Singleton<AudioManager>
{

    protected AudioManager() { }
    
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        var existing = FindObjectsOfType<Singleton<AudioManager>>();
        foreach (var exist in existing)
        {
            if (exist.GetComponent<AudioManager>() != Instance)
                Destroy(exist);
        }
    }

    public enum Sound
    {
        kill,
        die,
        digIn,
        digOut,
        click,
        combo2,
        combo3,
        combo4,
        combo5,
        comboLoose
    }
    void Start()
    {
        _sounds = new Dictionary<Sound, GameObject>();
        foreach (SoundEntry sound in Sounds)
        {
            GameObject audioClip = new GameObject();
            audioClip.AddComponent<AudioSource>();
            audioClip.GetComponent<AudioSource>().clip = sound.AudioClip;
            audioClip.name = sound.Sound.ToString();
            audioClip.transform.SetParent(transform);
            if (!_sounds.ContainsKey(sound.Sound))
                _sounds.Add(sound.Sound, audioClip);
        }
    }

    [Serializable]
    public class SoundEntry
    {
        public Sound Sound;
        public AudioClip AudioClip;

    }

    public List<SoundEntry> Sounds = new List<SoundEntry>();

    private Dictionary<Sound, GameObject> _sounds = new Dictionary<Sound, GameObject>();

    public object Debug { get; internal set; }

    public void PlaySound(Sound sound)
    {
        if (!_sounds.ContainsKey(sound))
            return;
        _sounds[sound].GetComponent<AudioSource>().Play();
    }
}
