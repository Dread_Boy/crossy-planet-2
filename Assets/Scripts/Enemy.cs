﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(RotateAroundPlanet))]
public class Enemy : MonoBehaviour
{
    FlyOff flyOff = null;

    public State state { get; private set; }

    void Start()
    {
        flyOff = GetComponent<FlyOff>();
        state = State.Alive;
    }

    public void Die()
    {
        if (flyOff != null)
        {
            GetComponent<Collider2D>().enabled = false;
            flyOff.flyOff(2f);
            StartCoroutine(Die(2f));
        }
        else
            Destroy(gameObject);
        state = State.Dying;
    }


    IEnumerator Die(float timeout)
    {
        yield return new WaitForSeconds(timeout);
        Destroy(gameObject);
    }

    public enum State
    {
        Incoming,
        Alive,
        Dying
    }

}
