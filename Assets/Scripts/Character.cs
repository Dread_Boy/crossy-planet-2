﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(RotateAroundPlanet))]
public class Character : MonoBehaviour
{

    RotateAroundPlanet planetRotation;
    public ScreenShake screenShake;
    public ComboManager comboManager;
    public DeathScreen deathScreen;
    public SpawnEnemy spawning;

    new BoxCollider2D collider2D;

    public State state = State.run;

    Vector3 tunnelIn;
    Vector3 tunnelOut;
    float tunnelTime;

    bool lastTap = false;

    void Start()
    {
        planetRotation = GetComponent<RotateAroundPlanet>();
        collider2D = GetComponent<BoxCollider2D>();
        if (comboManager == null)
            comboManager = FindObjectOfType<ComboManager>();
        if (deathScreen == null)
            deathScreen = FindObjectOfType<DeathScreen>();
        if (spawning == null)
            spawning = FindObjectOfType<SpawnEnemy>();
    }

    void FixedUpdate()
    {
        bool tap = checkTap();

        if (state == State.die)
            return;
        if (tap && state == State.run)
        {
            TunnelIn();
            return;
        }
        if (planetRotation.freezed && state == State.tunnel)
        {
            TunnelThrough();
            return;
        }
        if (!planetRotation.freezed && state == State.tunnel)
        {
            TunnelOut();
            return;
        }
    }

    private bool checkTap()
    {
        var tap = Input.GetAxis("Tap") > 0 || Input.touchCount > 0;
        var isTouch = tap && !lastTap;
        lastTap = tap;
        return isTouch;

    }

    void TunnelThrough()
    {
        var t = Time.time - tunnelTime;
        t = 5.63636f * Mathf.Pow(t, 4) - 8.79697f * Mathf.Pow(t, 3) + 3.33182f * Mathf.Pow(t, 2) + 0.828788f * t;
        transform.position = Vector3.Lerp(tunnelIn, tunnelOut, t);
    }

    void TunnelIn()
    {
        planetRotation.Freeze(1);
        state = State.tunnel;
        transform.Translate(0, 0, -2);
        transform.Rotate(new Vector3(0, 0, 1), -180);

        if (screenShake != null)
        {
            var shake = planetRotation.toTheCenter;
            shake.Normalize();
            shake /= -2;
            screenShake.Shake(shake);
        }

        AudioManager.Instance.PlaySound(AudioManager.Sound.digIn);


        tunnelIn = planetRotation.transform.position;
        tunnelIn += new Vector3(0, 0, -2);
        tunnelOut = planetRotation.GetOppositePosition(tunnelIn);
        tunnelOut += new Vector3(0, 0, -2);
        tunnelTime = Time.time;
    }

    void TunnelOut()
    {
        planetRotation.Rotate(Mathf.PI);
        state = State.run;

        AudioManager.Instance.PlaySound(AudioManager.Sound.digOut);

        comboManager.TunnelOut();

        spawning.SpawnOne();
    }

    public enum State
    {
        run,
        tunnel,
        die
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            if (state == State.tunnel)
            {
                AudioManager.Instance.PlaySound(AudioManager.Sound.kill);
                Enemy enemy = other.GetComponent<Enemy>();
                if (enemy != null)
                {
                    enemy.Die();
                    comboManager.Kill();
                }
            }
            else if (state == State.run)
            {
                StartCoroutine(StartDying());
            }
        }
        else if (other.tag == "Coin")
        {
            Destroy(other.gameObject);
            GameScore.Instance.Coins++;
        }
    }

    public void Die()
    {
        if (state != State.die)
            StartCoroutine(StartDying());
    }

    IEnumerator StartDying()
    {
        //Debug.Log("Start Dying");
        var chars = FindObjectsOfType<RotateAroundPlanet>();
        foreach (var character in chars)
            character.Freeze();
        state = State.die;
        spawning.Stop();

        AudioManager.Instance.PlaySound(AudioManager.Sound.die);
        GameScore.Instance.saveScore();
        comboManager.Reset();
        GetComponent<FlyOff>().flyOff(1);
        yield return new WaitForSeconds(1);
        if (deathScreen)
            deathScreen.Show(this);
    }

    public void Respawn()
    {
        comboManager.Reset();
        var chars = FindObjectsOfType<RotateAroundPlanet>();
        foreach (var character in chars)
            if (character.GetComponent<Enemy>() != null)
                DestroyImmediate(character.gameObject);
        if (planetRotation != null)
            planetRotation.Unfreeze();
        state = State.run;
        spawning.SpawnOne();
    }
}
