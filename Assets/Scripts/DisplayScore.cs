﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
[RequireComponent(typeof(Animator))]
public class DisplayScore : MonoBehaviour
{

    private Text text;
    private Animator animator;
    ComboColors comboColors = new ComboColors();
    private int score;
    private int combo;
    private Color color;

    void Start()
    {
        text = GetComponent<Text>();
        animator = GetComponent<Animator>();
        score = 0;
        combo = 0;
        GameScore.Instance.Combo = 0;
        color = text.color;
    }

    void Update()
    {
        if (score == GameScore.Instance.Score && combo == GameScore.Instance.Combo)
            return;
        if (animator != null && score != GameScore.Instance.Score)
            animator.SetTrigger("Increase");
        text.text = "Score: " + GameScore.Instance.Score;
        var index = GameScore.Instance.Combo - 2;
        if (index >= 15)
            index = 14;
        if (index >= 0)
            text.color = comboColors.colors[index];
        else
            text.color = color;
        score = GameScore.Instance.Score;
        combo = GameScore.Instance.Combo;
    }
}
