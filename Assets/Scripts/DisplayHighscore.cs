﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class DisplayHighscore : MonoBehaviour
{

    private Text text;
    private int score;

    void Start()
    {
        text = GetComponent<Text>();
        score = GameScore.Instance.getScore();
    }

    void Update()
    {
        text.text = "Your best: " + score.ToString();
    }
}
