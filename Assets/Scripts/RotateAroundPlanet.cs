﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class RotateAroundPlanet : MonoBehaviour
{
    public Planet planet;
    public float speed = 2;

    public Transform CenterOfGravity { get; private set; }
    public Vector3 toTheCenter { get; private set; }

    float time = 0;
    float timeRaw = 0;
    public float timeInit = 0;
    private float timeRotation = 0;

    private IEnumerator freezing;
    public bool freezed;

    public Direction direction = Direction.clockwise;

    void Start()
    {
        CenterOfGravity = planet.GetComponent<Transform>();
        if (CenterOfGravity == null)
            return;
        if (timeInit == 0)
        {
            timeInit = Mathf.Atan2(transform.position.y, transform.position.x) % (Mathf.PI * 2);
            //because programming languages don't obey mathematics
            if (timeInit < 0)
                timeInit += Mathf.PI * 2;
        }
        else
        {
            transform.position = new Vector3(planet.radius * Mathf.Cos(timeInit), planet.radius * Mathf.Sin(timeInit)) + CenterOfGravity.position;
            transform.rotation = Quaternion.Euler(0, 0, 57.29578f * timeInit - 90);
        }
    }
    
    public Vector2 GetPosition(float customTime)
    {
        if (CenterOfGravity == null)
            return Vector2.zero;
        return new Vector2(planet.radius * Mathf.Cos(customTime), planet.radius * Mathf.Sin(customTime)) + (Vector2)CenterOfGravity.position;
    }

    public Vector2 GetPosition()
    {
        RecalculateTime();
        return new Vector2(planet.radius * Mathf.Cos(time), planet.radius * Mathf.Sin(time)) + (Vector2)CenterOfGravity.position;
    }

    public Quaternion GetRotation(float customTime)
    {
        return Quaternion.Euler(0, 0, 57.29578f * customTime - 90);
    }

    /// <summary>
    /// Position relative to planet NOTE: planet might not be in center
    /// </summary>
    public Vector2 GetOppositePosition(Vector2 position)
    {
        return (CenterOfGravity.position - transform.position) * 2 + transform.position;
    }

    void RecalculateTime()
    {
        time = (int)direction * speed * timeRaw + timeInit + timeRotation;
    }

    void FixedUpdate()
    {
        if (CenterOfGravity == null)
            return;
        if (freezed)
            return;

        timeRaw += Time.deltaTime;
        RecalculateTime();

        toTheCenter = CenterOfGravity.position - transform.position;

        transform.position = new Vector3(planet.radius * Mathf.Cos(time), planet.radius * Mathf.Sin(time)) + CenterOfGravity.position;
        transform.rotation = Quaternion.Euler(0, 0, 57.29578f * time - 90);
    }

    public enum Direction
    {
        clockwise = 1,
        counterclockwise = -1
    }

    public void Freeze()
    {
        freezed = true;
    }

    public void Freeze(float freeze)
    {
        freezed = true;
        freezing = WaitForUnfreeze(freeze);
        StartCoroutine(freezing);
    }

    public void Unfreeze()
    {
        freezed = false;
        if (freezing != null)
            StopCoroutine(freezing);
    }

    public void Rotate(float angle)
    {
        //Debug.Log("Rotate");
        timeRotation += angle;
        FixedUpdate();
    }

    IEnumerator WaitForUnfreeze(float freeze)
    {
        yield return new WaitForSeconds(freeze);
        Unfreeze();
    }

    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.yellow;
        for (int i = 0; i < 40; i++)
        {
            Gizmos.DrawSphere(GetPosition(2 * Mathf.PI / 40 * i), 0.05f);
        }
    }
}
