﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Soundtrack : Singleton<Soundtrack>
{
    protected Soundtrack() { }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public object Debug { get; internal set; }

    int currentlyPlaying = -1;
    private List<GameObject> gameObjects = new List<GameObject>();
    public List<AudioClip> Tracks = new List<AudioClip>();

    void Start()
    {
        foreach (AudioClip sound in Tracks)
        {
            GameObject audioClip = new GameObject();
            audioClip.AddComponent<AudioSource>();
            audioClip.GetComponent<AudioSource>().clip = sound;
            audioClip.GetComponent<AudioSource>().loop = true;
            audioClip.name = sound.ToString();
            audioClip.transform.SetParent(transform);
            gameObjects.Add(audioClip);
        }

        PlaySoundtrack();
    }




    public void PlaySoundtrack()
    {
        if (Tracks.Count == 0)
            return;
        currentlyPlaying = UnityEngine.Random.Range(0, Tracks.Count - 1);
        gameObjects[currentlyPlaying].GetComponent<AudioSource>().Play();
    }

}
