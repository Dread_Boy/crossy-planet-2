﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour
{
    private RotateAroundPlanet rotate;
    Vector3 start;
    Vector3 end;
    float startTime;
    public float flyTime;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void FlyIn(float time)
    {
        start = end - rotate.toTheCenter * rotate.planet.orbit;
        end = rotate.GetPosition(time);
        transform.position = start;
        startTime = Time.time;
    }

    void FixedUpdate()
    {
        if (startTime > 0 && Time.time - startTime < flyTime)
            Flying();
    }

    void Flying()
    {
        transform.position = Vector3.Lerp(start, end, (end - start).magnitude / flyTime * (Time.time - startTime));
    }
}
