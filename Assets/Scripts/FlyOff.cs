﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(RotateAroundPlanet))]
public class FlyOff : MonoBehaviour
{
    private RotateAroundPlanet rotate;
    Vector3 start;
    Vector3 end;
    float startTime = -1;
    float time;

    void Start()
    {
        rotate = GetComponent<RotateAroundPlanet>();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="time">How far away should entity fly off?</param>
    public void flyOff(float time)
    {
        //Debug.Log("Fly off");
        rotate.Freeze();
        start = rotate.GetPosition();
        //transform.position = start;
        end = start - rotate.toTheCenter * time;
        startTime = Time.time;
        this.time = time;
    }

    void FixedUpdate()
    {
        if (startTime > -1 && Time.time - startTime < time)
            Flying();
    }

    void Flying()
    {
        transform.position = Vector3.Lerp(transform.position, end, 0.04f);
    }
}
