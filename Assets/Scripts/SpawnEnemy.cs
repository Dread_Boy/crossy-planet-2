﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SpawnEnemy : MonoBehaviour
{

    public Planet planet;
    public GameObject enemyPrefab;
    public GameObject arrowPrefab;

    Queue<SpawnInfo> spawnInfos = new Queue<SpawnInfo>();
    SpawnInfo lastSpawnInfo;

    float timeSinceStart = 0;

    bool paused = false;

    private IEnumerator spawnOne;

    void Awake()
    {
        spawnOne = _SpawnOne();
    }

    //IEnumerator SpawnRepeat()
    //{
    //    if (paused)
    //        yield break;
    //    PrepareSpawn();
    //    yield return new WaitForSeconds(waitTime);
    //    StartCoroutine(SpawnRepeat());
    //    yield return new WaitForSeconds(3 - waitTime);
    //    Spawn();
    //}

    IEnumerator _SpawnOne()
    {
        if (paused)
            yield break;
        var key = PrepareSpawn();
        yield return new WaitForSeconds(3);
        Spawn(key);
    }

    float PrepareSpawn()
    {
        if (arrowPrefab == null)
            throw new Exception("Your forgot spawning indicator prefab");

        var spawnPosition = UnityEngine.Random.Range(0, 2 * Mathf.PI);
        var arrow = Instantiate(arrowPrefab);
        var bobbing = arrow.GetComponent<ArrowBobbing>();
        bobbing.planet = planet;
        bobbing.spawn = spawnPosition;

        lastSpawnInfo.gameObject = arrow;
        lastSpawnInfo.time = spawnPosition;

        spawnInfos.Enqueue(lastSpawnInfo);
        return spawnPosition;
    }

    void Spawn(float key)
    {
        if (enemyPrefab == null)
            return;
        if (spawnInfos.Count < 0)
            return;
        if (paused)
            return;

        var spawn = spawnInfos.Dequeue();

        var enemy = Instantiate(enemyPrefab);
        var rotate = enemy.GetComponent<RotateAroundPlanet>();
        rotate.planet = planet;

        rotate.timeInit = spawn.time;

        var maxSpeed = 0.4f + 1.6f / 240f * Time.time;
        if (maxSpeed > 2.0f)
            maxSpeed = 2.0f;
        var minSpeed = 0.2f;

        rotate.speed = UnityEngine.Random.Range(minSpeed, maxSpeed);

        //var enemy2 = Instantiate(enemyPrefab);
        //var rotate2 = enemy2.GetComponent<RotateAroundPlanet>();
        //rotate2.planet = rotate.planet;
        //rotate2.timeInit = rotate.timeInit;
        //rotate2.speed = rotate.speed;

        Destroy(spawn.gameObject);
    }

    public void SpawnOne()
    {
        paused = false;
        var s = _SpawnOne();
        lastSpawnInfo = new SpawnInfo() { coroutine = s };
        StartCoroutine(_SpawnOne());
    }

    public void Stop()
    {
        paused = true;
        foreach (var pos in spawnInfos)
        {
            Destroy(pos.gameObject);
            StopCoroutine(pos.coroutine);
        }
        spawnInfos.Clear();
        //if (spawnOne != null)
        //    StopCoroutine(spawnOne);
    }

    public void ClearEnemies()
    {
        foreach (var enemy in FindObjectsOfType<Enemy>())
            DestroyImmediate(enemy.gameObject);
    }

    private struct SpawnInfo
    {
        public float time;
        public GameObject gameObject;
        public IEnumerator coroutine;
    }
}
