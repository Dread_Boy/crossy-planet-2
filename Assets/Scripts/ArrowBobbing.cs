﻿using UnityEngine;
using System.Collections;

public class ArrowBobbing : MonoBehaviour
{

    public Planet planet;
    public Transform CenterOfGravity { get; private set; }
    public Vector3 toTheCenter { get; private set; }
    Vector3 originalPosition;
    public float spawn;
    float startTime;
    float time;

    SpriteRenderer spriteRenderer = null;

    void Start()
    {
        CenterOfGravity = planet.GetComponent<Transform>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (CenterOfGravity == null || spriteRenderer == null)
            return;

        originalPosition = transform.position = new Vector3(planet.orbit * Mathf.Cos(spawn), planet.orbit * Mathf.Sin(spawn)) + CenterOfGravity.position;

        toTheCenter = CenterOfGravity.position - transform.position;
        transform.up = toTheCenter;

        startTime = Time.time;
    }

    void Update()
    {
        time = Time.time - startTime;
        transform.position = (Mathf.Sin((time - Mathf.PI / 2) * 5) / 3 * transform.up) + originalPosition;
        spriteRenderer.color = new Color(1f, 1f, 1f, time / 3f);

    }
}
