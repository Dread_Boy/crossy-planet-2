﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
[RequireComponent(typeof(Animator))]
public class MultiKill : MonoBehaviour {

    Text text = null;
    Animator animator = null;

    protected virtual void Start()
    {
        text = GetComponent<Text>();
        animator = GetComponent<Animator>();
    }
	
    public void Display()
    {
        animator.SetTrigger("Enter");
    }
}
