﻿using UnityEngine;

internal class ComboColors
{

    public Color[] colors = new Color[15];

    public ComboColors()
    {
        for (int i = 0; i < colors.Length; i++)
            colors[i] = Color.Lerp(new Color(218 / 255f, 181 / 255f, 102 / 255f), new Color(255 / 255f, 0 / 255f, 0 / 255f), i / 14.0f);
    }
}