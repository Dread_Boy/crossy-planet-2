﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class ComboKill : MonoBehaviour {

    Text text = null;
    Animator animator = null;
    bool shown = false;
    ComboColors comboColors = new ComboColors();

    void Start () {
        text = GetComponentInChildren<Text>();
        animator = GetComponent<Animator>();
        text.text = "";
	}

    public void Combo(int combo, int startCombo)
    {
        if (text == null)
            return;
        text.text = combo + " COMBO";
        animator.SetTrigger("combo");
        var index = combo - startCombo;
        if (index >= 15)
            index = 14;
        text.color = comboColors.colors[index];
        shown = true;
    }

    public void Reset()
    {
        if (text == null)
            return;
        if (!shown)
            return;
        animator.SetTrigger("reset");
        StartCoroutine(clearText());
        shown = false;
    }

    private IEnumerator clearText()
    {
        yield return new WaitForSeconds(0.36f);
        text.text = "";
    }
}
