﻿using UnityEngine;
using System.Collections;

public class SceneFlow : MonoBehaviour {

    public Menu menu;
    public ApplicationExit exit;
    public Character character;
    public Planet planet;
    public DisplayScore score;

	void Start () {
	
	}
	
	void Update () {
	
	}

    public void FromMenuToGame()
    {
        menu.gameObject.SetActive(false);
        exit.gameObject.SetActive(false);

        character.gameObject.SetActive(true);
        planet.gameObject.SetActive(true);
        score.gameObject.SetActive(true);
        GameScore.Instance.Reset();
        character.Respawn();
    }
    public void FromGameToMenu()
    {
        menu.gameObject.SetActive(true);
        exit.gameObject.SetActive(true);

        character.gameObject.SetActive(false);
        planet.gameObject.SetActive(false);
        score.gameObject.SetActive(false);
    }
}
