﻿using UnityEngine;
using System.Collections;
using System;
public class GameScore : Singleton<GameScore>
{

    public int Score = 0;
    public int Combo = 0;
    public int Coins = 0;

    public void Reset()
    {
        Score = 0;
        Combo = 0;
    }

    public void saveScore()
    {
        var current = getScore();
        if (Score > current)
            PlayerPrefs.SetInt("GameScore", Score);
    }


    public int getScore()
    {
        return PlayerPrefs.GetInt("GameScore");
    }
}
