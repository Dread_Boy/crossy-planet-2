﻿using UnityEngine;
using System.Collections;

public class Highscore : MonoBehaviour {

    Animator animator;
    bool shown = false;

    void Start () {
        animator = GetComponent<Animator>();

    }

    void Update () {
	
	}


    public void ShowHighscore()
    {

        if (animator != null)
        {
            animator.SetTrigger("Show");
            shown = true;
        }
    }

    public void HideHighscore()
    {
        if (animator == null)
            return;
        if (shown)
        {
            shown = false;
            animator.SetTrigger("Hide");
        }
    }
}
