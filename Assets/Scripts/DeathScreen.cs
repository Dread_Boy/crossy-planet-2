﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;

public class DeathScreen : MonoBehaviour
{

    Animator animator;
    Character character;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Show(Character character)
    {
        if (animator != null)
            animator.SetTrigger("Show");
        this.character = character;
    }

    public void Hide()
    {
        if (animator != null)
            animator.SetTrigger("Hide");
    }

    public void TryAgain()
    {
        GameScore.Instance.Reset();
        RespawnForReal();
    }

    public void Continue()
    {
        if (character == null)
            return;
        UnityEvent e = new UnityEvent();
        e.AddListener(RespawnForReal);
        Shop.Instance.SpendCoins(30, e);
    }

    void RespawnForReal()
    {
        Hide();
        character.Respawn();
    }

    void HandleAdResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                RespawnForReal();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}
