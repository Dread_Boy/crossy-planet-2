﻿using UnityEngine;
using System.Collections;

public class ScreenShake : MonoBehaviour {

    public Transform camTransform;
    private float journeyLength;
    Vector3 originalPos;
    private float startTime;
    public float speed = 1.0F;

    void Awake()
    {
        if (camTransform == null)
        {
            camTransform = GetComponent<Transform>();
        }
    }

    void OnEnable()
    {
        originalPos = camTransform.localPosition;
    }

    public void Shake(Vector3 direction)
    {
        camTransform.position = originalPos + direction;
        startTime = Time.time;
        journeyLength = Vector3.Distance(camTransform.position, originalPos);
    }

    void Update()
    {
        if (transform.position == originalPos)
            return;
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        transform.position = Vector3.Lerp(camTransform.position, originalPos, fracJourney);
    }
}
