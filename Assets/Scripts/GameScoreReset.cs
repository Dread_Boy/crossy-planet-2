﻿using UnityEngine;
using System.Collections;

public class GameScoreReset : MonoBehaviour {

	public void Reset()
    {
        GameScore.Instance.Reset();
    }
}
