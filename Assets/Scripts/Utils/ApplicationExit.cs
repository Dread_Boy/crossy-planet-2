﻿using UnityEngine;
using System.Collections;

public class ApplicationExit : MonoBehaviour
{

    public void Exit()
    {
        Application.Quit();
        Debug.LogWarning("Application Quit");
    }
}
