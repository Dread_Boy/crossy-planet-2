﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class BackButton : MonoBehaviour
{

    public UnityEvent Actions;

    void FixedUpdate()
    {
        if (Input.GetAxis("Cancel") > 0)
            Actions.Invoke();
    }
}
