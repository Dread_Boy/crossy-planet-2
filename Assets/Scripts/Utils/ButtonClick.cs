﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonClick : MonoBehaviour {

    void Start()
    {
        var button = transform.GetComponent<Button>();
        button.onClick.AddListener(onClick);
    }

	void onClick()
    {
        AudioManager.Instance.PlaySound(AudioManager.Sound.click);
    }
}
