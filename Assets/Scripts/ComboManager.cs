﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

public class ComboManager : MonoBehaviour
{

    private DoubleKill doubleKill = null;
    private TripleKill tripleKill = null;
    private UltraKill ultraKill = null;
    private Rampage rampage = null;
    private ComboKill comboKill = null;

    int killSinceTunnel = 0;
    int comboCount = 0;

    void Start()
    {
        doubleKill = FindObjectOfType<DoubleKill>();
        tripleKill = FindObjectOfType<TripleKill>();
        ultraKill = FindObjectOfType<UltraKill>();
        rampage = FindObjectOfType<Rampage>();
        comboKill = FindObjectOfType<ComboKill>();
    }

    public void Kill()
    {
        killSinceTunnel++;
        //streak
    }

    public void TunnelOut()
    {
        //double, triple kill
        if (killSinceTunnel == 2)
            doubleKill.Display();
        else if (killSinceTunnel == 3)
            tripleKill.Display();
        else if (killSinceTunnel == 4)
            ultraKill.Display();
        else if (killSinceTunnel >= 5)
            rampage.Display();

        //if we kill anything in that jump
        if (killSinceTunnel > 0)
        {
            if (comboCount > 0)
                GameScore.Instance.Score += comboCount;
            else
                GameScore.Instance.Score += killSinceTunnel;
            comboCount += killSinceTunnel;
            GameScore.Instance.Combo = comboCount;
            killSinceTunnel = 0;
        }
        //if we didn't kill anything
        else
        {
            //if there's no enemies alive on planet, don't loose kill streak
            var aliveEnemies = FindObjectsOfType<Enemy>().Where(e => e.state == Enemy.State.Alive);
            if (aliveEnemies.Count() == 0)
                return;
            if (comboCount > 0)
            {
                comboKill.Reset();
                AudioManager.Instance.PlaySound(AudioManager.Sound.comboLoose);
            }
            comboCount = 0;
            GameScore.Instance.Combo = comboCount;
        }

        if (comboCount >= 2)
        {
            comboKill.Combo(comboCount, 2);
            if (comboCount == 2)
                AudioManager.Instance.PlaySound(AudioManager.Sound.combo2);
            else if (comboCount == 3)
                AudioManager.Instance.PlaySound(AudioManager.Sound.combo3);
            else if (comboCount == 4)
                AudioManager.Instance.PlaySound(AudioManager.Sound.combo4);
            else if (comboCount >= 5)
                AudioManager.Instance.PlaySound(AudioManager.Sound.combo5);
        }
    }

    internal void Reset()
    {
        killSinceTunnel = 0;
        comboCount = 0;
        comboKill.Reset();
    }
}
